# For autoinstall

# your username
FUSERNAME="USER"

# your password
FUSERPASS="PASSWORD"

# root password
FROOTPASS="PASSWORD"

# The device's name
FHOSTNAME="Linux"

# Your location (Format like this, default is Europe/Budapest)
FUSERLOCA="Europe/Budapest"

# Shall we use swap , set it false for no
FISSWAP="true"

# Swap file's size
FSWAPSIZE="4G"

# Location for swapfile
FSWAPFILE="/.swapfile"
