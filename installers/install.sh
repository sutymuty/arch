#!/bin/bash
#Arch linux installer

#get packages.list
echo "Getting package list"
wget https://gitlab.com/jGBlM6l8oy7K/arch/-/raw/master/installers/packages.list
chmod 777 packages.list

#edit it 
echo "Edit packages.list as you wish"
vim packages.list

#for autoinstall
wget https://gitlab.com/jGBlM6l8oy7K/arch/-/raw/master/installers/autoinstall.txt
chmod 777 autoinstall.txt

if [ -f /root/autoinstall.txt ]; then
	read -p 'Autoinstall available, do you want to use?' RAUTOINSTALL
	if [[ "$RAUTOINSTALL" == "yes" || "$RAUTOINSTALL" == "" ||  "$RAUTOINSTALL" == "y" ]]; then
		RAUTOINSTALL=true
		vim autoinstall.txt
	fi
fi

#partitioning
lsblk
echo "Which disk to install for? (default is /dev/sda)"
read DISK

echo "Start partitioning (boot and home is required\!)"
cfdisk $DISK

STATUS=1
while [ $STATUS -ne 0 ]; do
	#list partitions
	echo "Partitions"
	fdisk -l | grep /dev | sed '1d'
	if [ "$BOOT" == "" ]; then
	#read boot partition
		echo "What's the name of your boot partition (Format: /dev/sda?)?"
    		read BOOT
    	#check if the partition exists
   		TESTIT=`fdisk -l | grep $BOOT`
   	 	if [ "$TESTIT" == "" ]; then
      			echo "$BOOT partition not found"\!
      			BOOT=""
      			continue
    		fi
    	#is it seems correct (not sure if it's)
    		LASTCHAR=`echo "${BOOT: -1}"`
    		if [[ "$LASTCHAR" != [0-9] ]]; then
      			echo "Invalid partition: $BOOT"
      			BOOT=""
      			continue
    		fi
	fi
 	#read root partition
	echo "What's the name of your root partition (Format: /dev/sda?)?"
  	read HOMEDIR
  	#check if the partition exists
  	TESTIT=`fdisk -l | grep $HOMEDIR`
  	if [ "$TESTIT" == "" ]; then
    		echo "$HOMEDIR partition not found"\!
    		continue
  	fi
  	#is it seems correct (not sure if it's)
  	LASTCHAR=`echo "${HOMEDIR: -1}"`
  	if [[ "$LASTCHAR" != [0-9] ]]; then
    		echo "Invalid partition: $HOMEDIR"
    		continue
  	fi
  	#check if the 2 dirs not the same (if the same, starting the loop from the beginnig)
  	if [ "$BOOT" == "$HOMEDIR" ]; then
    		echo  "Boot and root partition can not be the same"\!
    		BOOT=""
    		HOMEDIR=""
    		continue
  	fi
  	#if everything seems good
  	if [[ "$BOOT" != "" && "$HOMEDIR" != "" && "$BOOT" != "$HOMEDIR" ]]; then
   	 	#confirm
    		echo  "BOOT: $BOOT"
   		echo "HOME: $HOMEDIR"
    		read -p "Is it ok? (write yes)" AWS
    		if [ "$AWS" == "yes" ]; then
      			STATUS=0
      			break	
    		else
      			#if not good
      			BOOT=""
      			HOMEDIR=""
      			continue
   	 	fi
  	fi
done


#Encryption
echo "Encypting partition"
cryptsetup --verbose --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random luksFormat $HOMEDIR
cryptsetup open --type luks $HOMEDIR cryptroot

#get device UUID
ROOTUUID=`blkid $HOMEDIR | grep UUID=\" | awk -F '"' '{print $2}'`

if [ "$ROOTUUID" == "" ]; then
	echo "Something went wrong, cant find root UUID."
	exit 1
fi

#formating 
mkfs.ext4 $BOOT
mkfs.ext4 /dev/mapper/cryptroot

#mounting 
mount /dev/mapper/cryptroot /mnt
mkdir /mnt/boot
mount $BOOT /mnt/boot


#install base
pacstrap /mnt base base-devel
genfstab -U -p /mnt >> /mnt/etc/fstab

#copy files
cp packages.list /mnt/root/
wget https://gitlab.com/jGBlM6l8oy7K/arch/-/raw/master/installers/second_shell.sh
sleep 1
chmod +x /root/second_shell.sh
sed -i "s|REPLACEME|$HOMEDIR|g" /root/second_shell.sh
sed -i "s|UUIDREPLACE|$ROOTUUID|g" /root/second_shell.sh
if [ "$RAUTOINSTALL" == "true" ]; then
	sed -i "s/ISTHISAUTO/true/g" /root/second_shell.sh
	mv /root/autoinstall.txt /mnt/root/autoinstall.txt
fi
mv /root/second_shell.sh /mnt/root/second_shell.sh


#starting second_shell.sh
arch-chroot /mnt/ "/root/second_shell.sh"

#deleting unused files
rm -f /mnt/root/second_shell.sh
rm -f /mnt/root/packages_and_details.list
rm -f /mnt/root/autoinstall.txt

#umounting 
echo "Umounting the drives"
sleep 5
umount -l /mnt/boot
umount -l /mnt/
cryptsetup close cryptroot

if [ "$RAUTOINSTALL" == "true" ]; then
	reboot
fi

exit 0
