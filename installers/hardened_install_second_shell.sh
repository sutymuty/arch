#!/bin/bash

#this variables should be replaced by the master script with sed
ROOTUID="REPLACEME1" 	#the uuid of the root disk
BOOTFROMUSBUUID="REPLACEME2" #the uuid of the usb boot(linux) disk

#variables
NAMEOFUSER=""		#the username
NAMEOOFTHEDEV=""	#the device's name

LOCALPLACE=""		#The localization

CONFIRMATION=""		#should be yes/y

PACKAGELIST=" gdm gnome gnome-disk-utility gnome-extra gnome-tweak-tool acpi bash-completion bc cmake cowsay ffmpeg firefox flashplugin fortune-mod htop jdk10-openjdk libreoffice linux-headers mc net-tools ninja openssh python-pip screen screenfetch sl steam ufw unrar unzip vim vlc x264 zip clamav linux-headers linux-hardened-headers "		#packages for install by pacman
FROMAUR=" vim-neocomplete-git all-repository-fonts proton dxvk-bin dxvk-win64-bin dxvk-win32-bin xboxdrv "		#list to install things from aur
AURFILE="/tmp/aur.sh"	#the aur script
USRPASS=""		#the user's pass for the aur install

#let's start
read -p "What should be the name of the user? " NAMEOFUSER
read -p "What should be the name of the compter? " NAMEOOFTHEDEV

#localization
while true; do
	read -p "What should be the localization? (Continent/City) " LOCALPLACE
	if [ -f /usr/share/zoneinfo/$LOCALPLACE ]; then
		read -p "Are you sure the localization $LOCALPLACE? " CONFIRMATION
		if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
			ln -sf /usr/share/zoneinfo/$LOCALPLACE /etc/localtime
			break
		fi
	else
		echo "Invalid location: $LOCALPLACE"
	fi
done

#sys clock
hwclock --systohc

#language
sed -i 's/.*#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
export LANG=en_US.UTF-8

#Hostname
echo "$NAMEOOFTHEDEV" > /etc/hostname
echo "127.0.0.1	$NAMEOOFTHEDEV" >> /etc/hosts
echo "127.0.0.1	localhost" >> /etc/hosts

#setting up users
echo "Password for root user"
passwd root

useradd -m -s /bin/bash $NAMEOFUSER
echo "Password for $NAMEOFUSER"
passwd $NAMEOFUSER
echo "$NAMEOFUSER ALL=(ALL) ALL" >> /etc/sudoers

#add multilib
echo "" >> /etc/pacman.conf
echo '[multilib]' >> /etc/pacman.conf
echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf

#community-testing
echo "" >> /etc/pacman.conf
echo '[community-testing]' >> /etc/pacman.conf
echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf

pacman -Syu

#security things
pacman -S apparmor wget --noconfirm 
 
 # hook script 
echo '#!/usr/bin/ash
run_hook() { 

    modprobe -a -q dm-crypt >/dev/null 2>&1

    modprobe loop
 
    [ "${quiet}" = "y" ] && CSQUIET=">/dev/null" 

    while [ ! -L '/dev/disk/by-id/RE1' ]; do 

        echo 'Waiting for USB' 
        sleep 1 
     done 

     cryptsetup open /dev/disk/by-id/RE1 cryptboot 

     mkdir -p /mnt

     mount /dev/mapper/cryptboot /mnt

     cryptsetup open /dev/disk/by-id/RE2 arch --header=/mnt/header.img
    umount /mnt
}' > /etc/initcpio/hooks/customencrypthook

sed -i "s|RE1|$BOOTFROMUSBUUID|g" /etc/initcpio/hooks/customencrypthook
sed -i "s|RE2|$ROOTUID|g" /etc/initcpio/hooks/customencrypthook

head -n -20 /usr/lib/initcpio/install/encrypt > /etc/initpcio/install/customencrypthook

#modify mkinitcpio.conf
sed -i 's|MODULES=()|MODULES=(loop)|g' /etc/mkinitcpio.conf
sed -i "s/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/HOOKS=(base udev autodetect modconf block customencrypthook lvm2 filesystems keyboard fsck)/g" /etc/mkinitcpio.conf

mkinitcpio -p linux
mkinitcpio -p linux-hardened

#bootloader
sed -i 's|GRUB_CMDLINE_LINUX=""|GRUB_CMDLINE_LINUX="apparmor=1 security=apparmor audit=1"|g' /etc/default/grub
sed -i '/GRUB_CMDLINE_LINUX="apparmor=1 security=apparmor audit=1"/a GRUB_ENABLE_CRYPTODISK=y' /etc/default/grub

systemctl enable apparmor
systemctl enable auditd

grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB --recheck

grub-mkconfig -o /boot/grub/grub.cfg

#other

#vimrc
echo "let g:neocomplete#enable_at_startup = 1" >> /home/$NAMEOFUSER/.vimrc
echo "set number" >> /home/$NAMEOFUSER/.vimrc
echo "syntax on" >> /home/$NAMEOFUSER/.vimrc
cp /home/$NAMEOFUSER/.vimrc /root/.vimrc
chown $NAMEOFUSER:$NAMEOFUSER /home/$NAMEOFUSER/.vimrc
chown root:root /root/.vimrc

#for games 
echo "export SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0" >> /etc/environment

#.bashrc
echo "alias ll='ls -la --color=auto'" >> /home/$NAMEOFUSER/.bashrc
echo "alias grep='grep --color=auto'" >> /home/$NAMEOFUSER/.bashrc
echo "alias l='ls --color=auto'" >> /home/$NAMEOFUSER/.bashrc
cp /home/$NAMEOFUSER/.bashrc /root/.bashrc
chown $NAMEOFUSER:$NAMEOFUSER /home/$NAMEOFUSER/.bashrc
chown root:root /root/.bashrc

#yay
sleep 1
pacman -Syyu
pacman -S git go --noconfirm
git clone https://aur.archlinux.org/yay.git /tmp/yay/
chmod 777 -R /tmp/yay/
chown $NAMEOFUSER:$NAMEOFUSER -R /tmp/yay/
su $NAMEOFUSER -c "cd /tmp/yay && makepkg -s PKGBUILD"
pacman -U /tmp/yay/*.xz --noconfirm

pacman -Syyu

#install nvidia is we can
if [[ $(lspci | grep -i nvidia) != "" ]]; then
    pacman -S nvidia lib32-nvidia-utils nvidia-settings --noconfirm
fi

#cpu microcode
AMD=`lscpu | grep -i amd`
if [ "$AMD" != "" ]; then
        pacman -S amd-ucode --noconfirm
else
        INTEL=`lscpu | grep -i intel`
        if [ "$INTEL" != "" ]; then
                pacman -S intel-ucode --noconfirm
        fi
fi
grub-mkconfig -o /boot/grub/grub.cfg

#generationg aur file
touch $AURFILE
echo '#!/bin/bash' > $AURFILE
echo "yay -S $FROMAUR --noconfirm" >> $AURFILE
echo 'exit 0' >> $AURFILE
chmod +x $AURFILE

su $NAMEOFUSER -c 'git config --global core.editor "vim"'
git config --global core.editor "vim"

pacman -S $PACKAGELIST --noconfirm

systemctl enable ufw
systemctl enable gdm

#swap 
read -p "Would you like to create a swap? " CONFIRMATION
if [[ "$CONFIRMATION" == "yes" || "$CONFIRMATION" == "y" ]]; then
                echo "Where do you want to store the swapfile? (default is /.swapfile)"
                read SWAPFILE
                if [ "$SWAPFILE" == "" ]; then
                        SWAPFILE='/.swapfile'
                fi
                echo "Size of the swap? (default is 2G) FORMAT: XG"
                read SWAPSIZE
                if [ "$SWAPSIZE" == "" ]; then
                        SWAPSIZE="2G"
                fi
                echo "Creating swap"
                fallocate -l $SWAPSIZE  $SWAPFILE
                chmod 600 $SWAPFILE
                mkswap $SWAPFILE
                swapon $SWAPFILE
                echo '# swap' >> /etc/fstab
                echo "$SWAPFILE      none    swap    defaults        0       2" >> /etc/fstab
fi

#aur
read -s -p "What is your password?" USRPASS
su - $NAMEOFUSER -c "echo $USRPASS | script -e -q -a '/tmp/tmp.txt' -c '/tmp/aur.sh'"

rm -f /tmp/tmp.txt
rm -f /tmp/aur.sh

su - $NAMEOFUSER


exit 0
