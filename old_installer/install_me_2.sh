#!/bin/bash
#$1 = hostname $2 = username $3 = home partition $4 = boot type

#colors
red=`tput setaf 1`
green=`tput setaf 2`
white=`tput setaf 6`
reset=`tput sgr0`

SCRIPTQUEUE="yay -S "

echo '#!/bin/bash' > $BACKUPSCRIPT

echo "${green}[+] 2. script started!${reset}"
#Get localization
STATUS=1
while [ $STATUS -ne 0 ]; do
    echo "${white}[?] Where are you? (Continent/City format"\!" E.g.: Europe/Budapest)${reset}"
    read WHERE
    if [ -f /usr/share/zoneinfo/$WHERE ]; then
      ln -sf /usr/share/zoneinfo/$WHERE /etc/localtime
      STATUS=0
      break
    else
      echo "${red}["\!"] Invalid location"\!"${reset}"
    fi
done

#Setting up clock
echo "${green}[-] Setting up clock${reset}"
hwclock --systohc

#language
echo "${green}[-] Setting up language${reset}"
sed -i 's/.*#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
export LANG=en_US.UTF-8

#Hostname
echo "${green}[-] Writing hostname${reset}"
echo $1 > /etc/hostname
echo "127.0.0.1 $1" >> /etc/hosts

#Grub
echo "${green}[-] Installing grub${reset}"
if [ "$4" == "legacy" ]; then
	pacman -S grub os-prober --noconfirm
	grub-install /dev/sda
else
	pacman -S grub efibootmgr dosfstools os-prober mtools --noconfirm
	grub-install --target=x86_64-efi  --bootloader-id=grub_uefi --recheck
fi

#Encryption
echo "${green}[-] Setting up Encryption${reset}"
sed -i "/GRUB_CMDLINE_LINUX=""/c\GRUB_CMDLINE_LINUX=\"cryptdevice=$3:cryptroot\"" /etc/default/grub
sed -i "s/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck)/g" /etc/mkinitcpio.conf
mkinitcpio -p linux
if [ "$4" == "legacy" ]; then
	grub-install --recheck /dev/sda
else
	grub-install --target=x86_64-efi  --bootloader-id=grub_uefi --recheck
fi
grub-mkconfig -o /boot/grub/grub.cfg

#NetworkManager
echo "${green}[-] Install NetworkManager${reset}"
pacman -S networkmanager network-manager-applet --noconfirm
systemctl enable NetworkManager

#DE
STATUS=1
while [ $STATUS -ne 0 ]; do
	echo "${white}[?] Choose Desktop enviroment"\!" (default: gnome)${reset}"
	echo "${red}xfce4${reset}"
	echo "${red}gnome${reset}"
	echo "${red}deepin${reset}"
	echo "${red}lxde${reset}"
	echo "${red}kde${reset}"
	echo "${red}cinnamon${reset}"
	echo "${red}nothing${reset}"
	echo "${red}i3${reset}"
	read -p "${white}[?] Which you want?${reset} " DE
	echo "${green}[-] DE: $DE${reset}"
	echo "${red}[?] Is it right?${reset}Y/n"
	read ANS
	if [[ "$ANS" == "" || "$ANS" == "y" || "$ANS" == "yes" ]]; then
		STATUS=0
		break;
	fi
done
case $DE in
  nothing*)
  echo "${red}["\!"] No DE will be installed!${reset}"
  ;;
  xfce*)
  echo "${green}[-] Install XFCE4 and LIGHTDM${reset}"
  pacman -S xfce4 xfce4-goodies lightdm lightdm-gtk-greeter-settings pulseaudio pavucontrol networkmanager network-manager-applet --noconfirm
  systemctl enable lightdm
  ;;
  deepin*)
  echo "${green}[-] Install DEEPIN and LIGHTDM${reset}"
  pacman -Sg deepin deepin-extra --noconfirm
  pacman -S lightdm lightdm-gtk-greeter-settings --noconfirm
  systemctl enable lightdm
  ;;
  cinnamon*)
  echo "${green}[-] Install CINNAMON and LIGHTDM${reset}"
  pacman -S cinnamon lightdm --noconfirm
  systemctl enable lightdm
  ;;
  gnome*)
  echo "${green}[-] Install GNOME and GDM${reset}"
  pacman -S gnome gnome-extra gdm gnome-tweak-tool --noconfirm
  systemctl enable gdm
  ;;
  lxde*)
  echo "${green}[-] Install LXDE and LIGHTDM${reset}"
  pacman -S lxde-common lxsession lxdm lightdm lightdm-gtk-greeter-settings --noconfirm
  pacman -Sg lxde-gtk3 --noconfirm
  systemctl enable lightdm
  ;;
  kde*)
  echo "${green}[-] Install KDE and SDDM${reset}"
  pacman -S sddm plasma kdegraphics-thumbnailers ffmpegthumbs kde-applications --noconfirm
  systemctl enable sddm
  ;;
  i3*)
  echo "${green}[-] Install I3 ${reset}"
  pacman -S bc xorg-server python-gobject xorg-xinit rofi compton lxappearance pulseaudio pulseaudio-alsa pulseaudio-bluetooth pulseaudio-jack pulseaudio-equalizer redshift i3status i3-gaps i3blocks pavucontrol xfce4-terminal feh thunar --noconfirm
  SCRIPTQUEUE+=" pulseaudio pulseaudio-alsa pulseaudiocontrol  pulseaudio-ctl pulseaudio-dlna pulseaudio-equalizer-ladspa   pulseaudio-equalizer pulseaudio-module-sndio pulseaudio-jack  pulseaudio-lirc "
  ;;
  *)
  echo "${red}["\!"] Install GNOME and GDM${reset}"
  pacman -S gnome gnome-extra gdm gnome-tweak-tool --noconfirm
  systemctl enable gdm
  ;;
esac

#Root Password
echo "${green}[-] Setting up ${red}ROOT${green} password${reset}"
passwd root

#Creating user
echo "${green}[-] Creating user: ${reset}$2"
useradd -s /bin/bash -m $2

#Password for new user
echo "${green}[-] Setting up password for ${red}$2${green} and add to sudoers the new user${reset}"
passwd $2
echo "$2 ALL=(ALL) ALL" >> /etc/sudoers

#Multilib
echo "${green}[-] Add multilib repo${reset}"
echo "" >> /etc/pacman.conf
echo '[multilib]' >> /etc/pacman.conf
echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf

#Install aurman
echo "${green}[-] Adding yay by yourself${reset}"
pacman -Syyu
pacman -S go --noconfirm
git clone https://aur.archlinux.org/yay.git  /tmp/yay
chown -R $2:$2 /tmp/yay
su - $2 

#betterlockscreen and themes
if [ "$DE" == "i3" ]; then
	echo "${green}[-] Install lockscreen and themes + add i3 to start${reset}"
	pacman -S arc-gtk-theme  --noconfirm
	SCRIPTQUEUE+=" betterlockscreen matcha-icon-theme pulseaudio-ctl "	
	echo "exec i3" >> /home/$2/.xinitrc
	echo "exec i3" >> /root/.xinitrc
	chown $2:$2 /home/$2/.xinitrc
	chmod 755 /home/$2/.xinitrc
fi 

#Useful apps
pacman -Syyu

sleep 5

echo "${green}[-] Installing...${reset}"
pacman -S screenfetch sl bc acpi htop python-pip ufw mc libreoffice firefox vim zip unzip openssh vlc flashplugin x264 unrar jdk10-openjdk bash-completion ffmpeg net-tools --noconfirm
SCRIPTQUEUE+=" all-repository-fonts "
systemctl enable ufw

#Touchpad driver and bluetooth
echo "${white}[?] Install touchpad and bluetooth things? ${reset} Y/n"
read AWS
if [[ "$AWS" == "yes" || "$AWS" == "y" || "$AWS" == "" ]]; then
  pacman -S xf86-input-synaptics blueman --noconfirm
fi

if [[ "$AWS" == "yes" || "$AWS" == "y" || "$AWS" == "" ]]; then
  pacman -S xf86-input-synaptics bluez-utils bluez --noconfirm
  systemctl enable bluetooth.service
fi

#xfce4 things
if [[ "$DE" == "xfce" || "$DE" == "xfce4" ]];  then
  echo "${white}[?] Do you want to download xfce4 theme and icons?${reset} Y/n"
  read AWS
  if [[ "$AWS" == "yes" || "$AWS" == "y" || "$AWS" == "" ]]; then
	SCRIPTQUEUE+=" matcha-gtk-theme matcha-icon-theme "
  fi
  echo "${white}[?] Install redshift?${reset}Y/n"
  read AWS
  if [[ "$AWS" == "yes" || "$AWS" == "y" || "$AWS" == "" ]]; then
    pacman -S redshift --noconfirm
  fi
fi

#videocard
ISITN1=`lspci | grep nvidia`
ISITN2=`lspci | grep NVIDIA`
if [[ "$ISITN1" != "" || "$ISITN2" != "" ]]; then
  echo "${white}[?] Do you want to NVIDIA drivers?${reset}Y/n"
  read AWS
  if [[ "$AWS" == "yes" || "$AWS" == "y" || "$AWS" == "" ]]; then
    pacman -S nvidia lib32-nvidia-utils nvidia-settings --noconfirm
  fi
fi

#steam
echo "${white}[?] Install steam?${reset}Y/n"
read AWS
if [[ "$AWS" == "yes" || "$AWS" == "y" || "$AWS" == "" ]]; then
	pacman -S steam --noconfirm
	SCRIPTQUEUE+=" proton "
fi 

#vim extensions
echo "${green}[-] Install vim-neocomplete-git${reset}"
if [ $? -ne 0 ]; then
	SCRIPTQUEUE+=" vim-neocomplete-git "
fi

echo "let g:neocomplete#enable_at_startup = 1" >> /home/$2/.vimrc
echo "set number" >> /home/$2/.vimrc
echo "syntax on" >> /home/$2/.vimrc
cp /home/$2/.vimrc /root/.vimrc
chown $2:$2 /home/$2/.vimrc
chown root:root /root/.vimrc

#bashrc
echo "${green}[-] Writing aliases to .bashrc${reset}"
echo "alias ll='ls -la --color=auto'" >> /home/$2/.bashrc
echo "alias grep='grep --color=auto'" >> /home/$2/.bashrc
echo "alias l='ls --color=auto'" >> /home/$2/.bashrc

cp /home/$2/.bashrc /root/.bashrc
chown $2:$2 /home/$2/.bashrc
chown root:root /root/.bashrc

#swap
echo "${white}[?] Do you want swap?${reset}Y/n"
read AWS
if [[ "$AWS" == "yes" || "$AWS" == "y" ]]; then
	echo "${white}[[?]]Where do you want to store the swapfile? (default is /.swapfile)${reset}"
	read SWAPFILE
	if [ "$SWAPFILE" == "" ]; then
		SWAPFILE='/.swapfile'
	fi
	echo "${white}[[?]]Size of the swap? (default is 2G) FORMAT: XG${reset}"
	read SWAPSIZE
	if [ "$SWAPSIZE" == "" ]; then
		SWAPSIZE="2G"
	fi
	echo "${green}[-] Creating swap${reset}"
	fallocate -l $SWAPSIZE  $SWAPFILE
	chmod 600 $SWAPFILE
	mkswap $SWAPFILE
	swapon $SWAPFILE
	echo "\# swap" >> /etc/fstab
	echo "$SWAPFILE      none    swap    defaults        0       0" >> /etc/fstab
	echo "${green}[-] Swap created"\!"${reset}"
fi	

if [ ! -f /home/$2/install_aur_things.sh ]; then
        touch /home/$2/install_aur_things.sh
fi

BACKUPSCRIPT=/home/$2/install_aur_things.sh

echo "$SCRIPTQUEUE --noconfirm" >> $BACKUPSCRIPT
chmod 777 $BACKUPSCRIPT

su - $2

exit 0
